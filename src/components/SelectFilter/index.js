import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'

import styles from './index.css'


export default class SelectFilter extends Component {

    static propTypes = {
        filters: PropTypes.arrayOf(PropTypes.shape({
            name: PropTypes.string,
            children: PropTypes.array,
        })).isRequired,
        onChange: PropTypes.func,
        rootStyle: PropTypes.string
    }

    state = {
        inputValue: null,
        filters: this.props.filters,
    }
    handleGenreClick = (name) => {
        const {
          onChange,
          filters,
        } = this.props;

        filters.map((filter) => {
            filter.classname = ''
            if (filter.name == name) {
                filter.classname = styles.checked
            }
            filter.children.map((child) => {
                child.classname = ''
            })
        })

        onChange(name);
    }
    handleClick = (childClicked) => {
        const {
          onChange,
          filters
        } = this.props;

        filters.map((filter) => {
            filter.classname = ''
            filter.children.map((child) => {
                if (child.name == childClicked.name) {
                    child.classname = styles.checked
                } else {
                    child.classname = ''
                }
            })
        })

        onChange(childClicked.name);
    }

    handleInputChange = (event) => {
        this.setState({
            inputValue: event.target.value,
        })
    }

    getFiltersBySearch = (inputValue) => {
        const {
          filters,
        } = this.props;

        let filtersFiltered = []
        const regex = new RegExp(`${inputValue}`, 'gi')

        filters.map((filter) => {
            let filtersWithChildren = []
            filter.children.map((child) => {
                if (child.name.match(regex)) {
                    filtersWithChildren.push(child)
                }
            })
            if (filtersWithChildren.length > 0) {
                filtersFiltered.push({
                    name: filter.name,
                    children: filtersWithChildren
                })
            }
        })
        return filtersFiltered
    }

    renderFilters() {
        const {
          rootStyle,
          filters,
        } = this.props;

        const {
          inputValue,
        } = this.state

        const filtersToDisplay = inputValue ? this.getFiltersBySearch(inputValue) : filters

        const filtersDisplay = filtersToDisplay.map((filter) => {
            const children = filter.children.map((child) => {
                return (<p className={ classNames(styles.filter, child.classname, rootStyle)} onClick={this.handleClick.bind(this, child)} key={child.name} >
                  {child.name}
                </p>)
            })
            return (
              <div className={ classNames(styles.filtercontainer)} key={`filter-child-${filter.name}`}>
                <p className={ classNames(styles.filterGenre)} onClick={this.handleGenreClick.bind(this, filter.name)}>{filter.name}</p>
                <div className="filter-children">
                    {children}
                </div>
            </div>
            )
        })
        return filtersDisplay
    }

    render() {

        const {
          inputValue,
        } = this.state;
        const style = {
            position: 'absolute',
            top: '8px',
            color: '#888888',
        }

        return (
            <div className={ classNames(styles.selectfilter)}>
                <p>Select a channel</p>
                <div className={ classNames(styles.inputcontainer)}>
                  <input className={ classNames(styles.input)} onChange={this.handleInputChange} placeholder="Search" value={inputValue || ''} />
                  <i className="icon-search" style={style} />
                </div>
                {this.renderFilters()}
            </div>
        )
    }
}
