import React, { Component } from 'react'
import request from 'superagent'

// import components
import SelectFilter from 'components/SelectFilter'

import styles from './index.css'


export default class Home extends Component {

    state = {
        data: null,
        filter: null,
    }

    componentDidMount() {

        // do your xhr request here (http://localhost:5000/category)
        request
            .get('http://localhost:5000/category')
            .end((err, res) => {
                if (res) {
                    this.setState({
                        data: res.body,
                    })
                }
            });
    }

    render() {

        const {
          data,
          filter,
        } = this.state

        return (
            <div className={ styles.home }>
                <h1>ifs test react</h1>
                <div className = { styles.selectcontainer }>
                  <SelectFilter
                      filters={data || []}
                      onChange={(currentFilter) => {
                          console.log('currentFilter', currentFilter) // eslint-disable-line
                          this.setState({
                              filter: currentFilter
                          })
                      }}
                      rootStyle={ styles.filter }
                  />
                  <p className={ styles.filterDisplay }>Filter by : {filter}</p>
              </div>
            </div>
        )
    }
}
